JS Lab: Use RxJS to stream mouse movement into a Leaflet map layer.
=

Enjoying the beauty of a quick map visual (albeit contrived) using RxJS Observables to paint geo-data onto a heatmap.

#### Technologies
RxJS and Leaflet.

#### View
Run a web server and open `index.html`.

#### Notes
Nothing important here.  Just appreciating RxJS.  Yep.

#### Todo
- Add pulldown to switch between adding points on the Heatmap or CusterLayer.