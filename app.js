var mono_south = [37.642, -118.967];

var map = L.map('mapid', {
}).setView(mono_south, 16);

var els = {
    count: document.querySelector('#count'),
    coords: document.querySelector('#center-coords'),
    buttonClear: document.querySelector('#button-clear'),
};

els.buttonClear.addEventListener('click', function (e) {
    clearData();
    e.stopPropagation();
    return false;
});

L.tileLayer('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '@ <a href="https://carto.com/attributions" target="_blank">CARTO</a>'
}).addTo(map);

var markers = L.markerClusterGroup({
    showCoverageOnHover: false,
    zoomToBoundsOnClick: false,
    spiderfyOnMaxZoom: false,
    removeOutsideVisibleBounds: false,
}).addTo(map);

var heat = L.heatLayer([], {
    radius: 25,
    blur: 25,
    gradient: {0.4: 'blue', 0.65: 'lime', 1: 'red'}
}).addTo(map);

Rx.Observable
    .fromEvent(map, 'mousemove')
    .subscribe(function (moveEvent) {
        setMousePosition(moveEvent);
        setData(moveEvent);
    });

function setData(event) {
    var latlng = event.latlng;
    // markers.addLayer(L.circleMarker(latlng, {
    //     fill: 'dodgerblue',
    // }));
    heat.addLatLng([latlng.lat, latlng.lng, 0.2]);
    els.count.innerHTML = heat._latlngs.length;
}

function clearData() {
    heat.setLatLngs([]);
    els.count.innerHTML = 0;
}

function setMousePosition(event) {
    els.coords.innerHTML = [event.latlng.lat.toFixed(5), event.latlng.lng.toFixed(5)].join(', ');
}